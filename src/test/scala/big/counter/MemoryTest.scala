package big.counter

import big.counter.data.config.CounterConfig
import it.unimi.dsi.fastutil.HashCommon
import net.agkn.hll.HLL
import org.scalameter.api._
import org.scalameter.picklers.Pickler
import org.scalameter.{Key, KeyValue}

import scala.collection.concurrent
import java.util.concurrent.ConcurrentHashMap
import scala.jdk.CollectionConverters._


object MemoryTest extends Bench.ForkedTime {
  override def persistor: Persistor = new SerializationPersistor
  override def measurer = new Executor.Measurer.MemoryFootprint

  val configs: Gen[(Int, Int)] = Gen.enumeration("hllConfig")(
    (16, 1), (16, 2), (16, 3), (16, 4),
    (17, 1), (17, 2), //(17, 3),
    (18, 1), //(19, 1), (20, 1)
  )

  performance of "HLL memory" in {
    using(configs) config (
      KeyValue(exec.benchRuns -> 1),
      KeyValue(exec.independentSamples -> 3),
      KeyValue(exec.jvmflags.asInstanceOf[Key[Any]] -> Seq("-Xmx5G", "-Xms2G")),
    ) in { c =>
      buildHllCopies(CounterConfig.tupled(c))
    }
  }

  def buildHllCopies(conf: CounterConfig): concurrent.Map[String, HLL] = {
    val h = new HLL(conf.registersLog, conf.registerWidth)

    1.to(100_000) foreach { i =>
      h.addRaw(HashCommon.murmurHash3(i))
    }

    val data = h.toBytes

    val map = new ConcurrentHashMap[String, HLL].asScala

    1.to(100_000) foreach { i =>
      val name = s"event_$i"
      map.put(name, HLL.fromBytes(data))
    }

    map
  }

  implicit lazy val intPairPickler: Pickler[(Int, Int)] = new Pickler[(Int, Int)] {
    override def pickle(x: (Int, Int)): Array[Byte] =
      Array[Byte](x._1.toByte, x._2.toByte)

    override def unpickle(a: Array[Byte], from: Int): ((Int, Int), Int) =
      ((a(from), a(from + 1)), from + 2)
  }
}
