package big.counter

import big.counter.data.config.CounterConfig
import it.unimi.dsi.fastutil.HashCommon
import net.agkn.hll.HLL
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import scala.collection.mutable
import scala.util.Random

class AccuracySpec extends AnyFlatSpec with should.Matchers {

  val expectedCount: Int = 100_000
  val samples = 10

  val configs: Seq[CounterConfig] =
    for {
      b <- 4.to(21)
      w <- 1.to(8)
    } yield CounterConfig(b, w)

  configs foreach { config =>
    s"HLL(${config.registersLog}, ${config.registerWidth})" should "have accuracy >99.9%" in {
      val acc = Iterable.fill(samples)(sampleAccuracy(config)).min
      assert(acc >= 99.9, s"Accuracy=$acc%")
    }
  }

  def sampleAccuracy(config: CounterConfig): Double = {
    val h = new HLL(config.registersLog, config.registerWidth)
    val used = mutable.Set.empty[Long]

    1.to(100_000) foreach { _ =>
      val nextUnique =
        LazyList.continually(Random.nextLong())
          .dropWhile(used.contains).head
      used.add(nextUnique)
      h.addRaw(HashCommon.murmurHash3(nextUnique))
    }
    val resultCount = h.cardinality
    100.0 * (1.0 - Math.abs(resultCount - expectedCount) /expectedCount.toDouble)
  }
}
