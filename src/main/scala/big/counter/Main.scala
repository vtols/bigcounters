package big.counter

import big.counter.data.Event
import big.counter.endpoints.AppRoutes
import big.counter.service._
import cats.effect.std.Queue
import org.http4s.server.blaze.BlazeServerBuilder
import org.log4s.getLogger
import zio._
import zio.interop.catz._

object Main extends zio.interop.catz.CatsApp {
  private val logger = getLogger

  private val configLoader = new Configs

  val program: RIO[ZEnv, Unit] =
    for {
      serverConfig    <- configLoader.loadServerConfig
      counterConfig   <- configLoader.loadCounterConfig
      consumerConfig  <- configLoader.loadConsumerConfig
      publisherConfig <- configLoader.loadPublisherConfig


      outgoingEvents <-
        Queue.unbounded[Task, Event]
      counter        <-
        UIO { new UniqueCounterImpl(counterConfig) }
      eventConsumer  =
        new EventConsumerImpl(consumerConfig, counter)
      eventPublisher =
        new EventPublisherImpl(publisherConfig, outgoingEvents)
      httpApp        =
        new AppRoutes(counter, outgoingEvents)
      server         =
        BlazeServerBuilder[Task](runtime.platform.executor.asEC)
          .bindHttp(serverConfig.port, serverConfig.host)
          .withHttpApp(httpApp.routes)
          .serve
      _              <-
        server
          .concurrently(eventPublisher.publishingStream)
          .concurrently(eventConsumer.consumingStream)
          .compile.drain
    } yield ()

  override def run(args: List[String]): URIO[ZEnv, ExitCode] =
    program.fold(
      failure => {
        logger.error(failure)("Application run failed")
        ExitCode.failure
      },
      _ => ExitCode.success
    )
}
