package big.counter.endpoints

import big.counter.data.Event
import big.counter.service.UniqueCounter
import cats.effect.std.Queue
import cats.syntax.flatMap._
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._
import org.http4s.{HttpApp, HttpRoutes}
import zio.Task
import zio.interop.catz._

class AppRoutes(counter: UniqueCounter,
                publishQueue: Queue[Task, Event])
  extends Http4sDsl[Task] {

  val routes: HttpApp[Task] =
    HttpRoutes.of[Task] {
      case request @ POST -> Root / "data" =>
        request.decodeJson[Event]
          .flatTap(counter.addEvent)
          .flatMap(publishQueue.offer)
          .flatMap(_ => Ok())
      case GET -> Root / "reports" =>
        Ok(counter.asMap.map(_.asJson))
    }.orNotFound
}