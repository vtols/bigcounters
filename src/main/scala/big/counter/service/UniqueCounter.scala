package big.counter.service

import big.counter.data.Event
import big.counter.data.config.CounterConfig
import it.unimi.dsi.fastutil.HashCommon
import net.agkn.hll.HLL
import zio.UIO

import java.util.concurrent.ConcurrentHashMap
import scala.collection.concurrent
import scala.jdk.CollectionConverters._

trait UniqueCounter {
  def addEvent(event: Event): UIO[Unit]

  def asMap: UIO[Map[String, Long]]
}

class UniqueCounterImpl(config: CounterConfig) extends UniqueCounter {
  private val countersDictionary: concurrent.Map[String, HLL] =
    new ConcurrentHashMap[String, HLL].asScala

  private def createCounter =
    new HLL(config.registersLog, config.registerWidth)

  override def addEvent(event: Event): UIO[Unit] =
    UIO {
      val counter = countersDictionary
        .getOrElseUpdate(event.eventName, createCounter)
      counter.synchronized {
        counter.addRaw(HashCommon.murmurHash3(event.value))
      }
    }

  override def asMap: UIO[Map[String, Long]] =
    UIO {
      Map.from(
        countersDictionary.iterator
          .map { case (k, v) => (k, v.synchronized { v.cardinality }) }
      )
    }
}
