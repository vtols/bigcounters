package big.counter.service

import big.counter.data.config.{CounterConfig, EventConsumerConfig, EventPublisherConfig, ServerConfig}
import cats.effect.Sync
import pureconfig.generic.auto._
import pureconfig.module.catseffect.syntax._
import pureconfig.{ConfigReader, ConfigSource}
import zio.Task

import scala.reflect.ClassTag

class Configs(implicit sync: Sync[Task]) {
  private def loadConfig[A](path: String)
                   (implicit reader: ConfigReader[A],
                    tag: ClassTag[A]): Task[A] =
    ConfigSource.default.at(path).loadF[Task, A]()

  val loadServerConfig: Task[ServerConfig] =
    loadConfig[ServerConfig]("server")

  val loadCounterConfig: Task[CounterConfig] =
    loadConfig[CounterConfig]("counter")

  val loadConsumerConfig: Task[EventConsumerConfig] =
    loadConfig[EventConsumerConfig]("kafka.consumer")

  val loadPublisherConfig: Task[EventPublisherConfig] =
    loadConfig[EventPublisherConfig]("kafka.producer")
}
