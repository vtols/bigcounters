package big.counter.service

import big.counter.data.Event
import big.counter.data.config.EventPublisherConfig
import cats.effect.std.Queue
import fs2.Stream
import fs2.kafka._
import zio.interop.catz._
import zio.{Runtime, Task, ZEnv}
import io.circe.syntax._

trait EventPublisher {
  def publishingStream: Stream[Task, Nothing]
}

class EventPublisherImpl(config: EventPublisherConfig,
                         publishQueue: Queue[Task, Event])
                        (implicit rt: Runtime[ZEnv]) extends EventPublisher {
  private val producerSettings: ProducerSettings[Task, Option[String], String] =
    ProducerSettings[Task, Option[String], String]
      .withBootstrapServers(config.bootstrapServersString)

  override val publishingStream: Stream[Task, Nothing] =
    Stream.fromQueueUnterminated(publishQueue)
      .mapAsyncUnordered(config.maxConcurrent) { event =>
        Task { event.asJson.noSpaces }
          .map(ProducerRecord[Option[String], String](config.topic, None, _))
          .map(ProducerRecords.one[Option[String], String])
      }
      .through(KafkaProducer.pipe(producerSettings))
      .drain
}
