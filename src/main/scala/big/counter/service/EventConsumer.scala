package big.counter.service

import big.counter.data.Event
import big.counter.data.config.EventConsumerConfig
import fs2.Stream
import fs2.kafka.{AutoOffsetReset, ConsumerSettings, KafkaConsumer}
import io.circe.parser.decode
import org.log4s.getLogger
import zio.interop.catz._
import zio.{Runtime, Task, ZEnv}

trait EventConsumer {
  def consumingStream: Stream[Task, Nothing]
}

class EventConsumerImpl(config: EventConsumerConfig,
                        counter: UniqueCounter)
                       (implicit rt: Runtime[ZEnv]) extends EventConsumer {

  import big.counter.coding.custom._

  private val logger = getLogger

  private val consumerSettings: ConsumerSettings[Task, Option[String], String] =
    ConsumerSettings[Task, Option[String], String]
      .withAutoOffsetReset(AutoOffsetReset.Earliest)
      .withBootstrapServers(config.bootstrapServersString)
      .withGroupId(config.consumerGroupId)

  override val consumingStream: Stream[Task, Nothing] =
    KafkaConsumer.stream(consumerSettings)
      .evalTap(_.subscribeTo("events"))
      .flatMap(_.stream)
      .mapAsyncUnordered(config.maxConcurrent) { event =>
        Task { decode[Event](event.record.value) }.flatMap {
          case Left(error) =>
            Task {
              logger.error(error)("Can't parse an incoming event")
            }
          case Right(value) =>
            logger.debug(value.toString)
            counter.addEvent(value)
        }
      }
      .drain
}
