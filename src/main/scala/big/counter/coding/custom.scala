package big.counter.coding

import io.circe.generic.AutoDerivation
import io.circe.generic.extras._

object custom extends AutoDerivation {
  implicit val config: Configuration = Configuration.default.withSnakeCaseMemberNames
}
