package big.counter.data

import io.circe.Codec
import io.circe.generic.extras.semiauto._

case class Event(eventName: String, value: Long)

object Event {
  import big.counter.coding.custom._

  implicit val eventEncoder: Codec[Event] = deriveConfiguredCodec
}