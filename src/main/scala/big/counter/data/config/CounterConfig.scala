package big.counter.data.config

case class CounterConfig(registersLog: Int,
                         registerWidth: Int)
