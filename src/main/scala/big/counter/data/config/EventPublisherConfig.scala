package big.counter.data.config

case class EventPublisherConfig(bootstrapServers: List[String],
                                topic: String,
                                maxConcurrent: Int)
  extends BootstrapServersConfig