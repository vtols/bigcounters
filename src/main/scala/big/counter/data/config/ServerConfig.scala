package big.counter.data.config

case class ServerConfig(host: String, port: Int)
