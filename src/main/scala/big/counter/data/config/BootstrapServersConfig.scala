package big.counter.data.config

trait BootstrapServersConfig {
  def bootstrapServers: List[String]

  def bootstrapServersString: String =
    bootstrapServers.mkString(",")
}
