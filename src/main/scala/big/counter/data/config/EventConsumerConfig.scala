package big.counter.data.config

case class EventConsumerConfig(bootstrapServers: List[String],
                               topic: String,
                               consumerGroupId: String,
                               maxConcurrent: Int)
  extends BootstrapServersConfig
