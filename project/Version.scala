object Version {
  val Log4s       = "1.10.0-M6"
  val PureConfig  = "0.15.0"
  val Zio         = "1.0.7"
  val CatsInterop = "3.0.2.0"
  val Http4s      = "1.0.0-M21"
  val Fs2         = "3.0.2"
  val Fs2Kafka    = "2.0.0"
  val Cats        = "2.6.0"
  val CatsEffect  = "3.1.0"
  val Circe       = "0.13.0"
}
