addCompilerPlugin("org.typelevel" % "kind-projector" % "0.11.3" cross CrossVersion.full)

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

lazy val root = (project in file("."))
  .settings(
    name := "BigCounters",
    scalaVersion := "2.13.5",

    scalacOptions ++= Seq(
      "-encoding", "utf8",
      "-deprecation",
      "-feature",
      "-Xfatal-warnings"
    ),

    Universal / javaOptions ++= Seq(
      "-J-Xmx2700m",
      "-Dserver.host=0.0.0.0"
    ),

    dockerExposedPorts ++= Seq(8080),

    libraryDependencies ++= Seq(
      "ch.qos.logback" % "logback-classic" % "1.2.3",

      "net.agkn" % "hll" % "1.6.0",
      "it.unimi.dsi" % "fastutil" % "6.5.11",

      "org.log4s" %% "log4s" % Version.Log4s,

      "com.github.pureconfig" %% "pureconfig"             % Version.PureConfig,
      "com.github.pureconfig" %% "pureconfig-cats-effect" % Version.PureConfig,

      "dev.zio" %% "zio"              % Version.Zio,
      "dev.zio" %% "zio-test"         % Version.Zio,

      "dev.zio" %% "zio-interop-cats" % Version.CatsInterop,

      "org.http4s" %% "http4s-core"         % Version.Http4s,
      "org.http4s" %% "http4s-client"       % Version.Http4s,
      "org.http4s" %% "http4s-blaze-server" % Version.Http4s,
      "org.http4s" %% "http4s-dsl"          % Version.Http4s,
      "org.http4s" %% "http4s-circe"        % Version.Http4s,

      "co.fs2" %% "fs2-core" % Version.Fs2,
      "co.fs2" %% "fs2-io"   % Version.Fs2,

      "org.typelevel" %% "cats-effect-std" % Version.CatsEffect,

      "com.github.fd4s" %% "fs2-kafka" % Version.Fs2Kafka,

      "org.typelevel" %% "cats-core" % Version.Cats,

      "io.circe" %% "circe-core"           % Version.Circe,
      "io.circe" %% "circe-parser"         % Version.Circe,
      "io.circe" %% "circe-generic"        % Version.Circe,
      "io.circe" %% "circe-generic-extras" % Version.Circe,

      "org.scalatest"     %% "scalatest"  % "3.2.7" % Test,
      "com.storm-enroute" %% "scalameter" % "0.21"  % Test,
    ),

    Test / classLoaderLayeringStrategy :=
      ClassLoaderLayeringStrategy.Flat,

    testFrameworks += new TestFramework(
      "org.scalameter.ScalaMeterFramework"),

    Test / parallelExecution := false
  )
